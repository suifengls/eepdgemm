#Energy Efficient Parallel Matrix-Matrix Multiplication#

PS: You should have the right to change the cpu frequency by writing /sys/devices/system/cpu/cpu[0-8]/cpufreq/scaling_setspeed

##Files in this repository##


* demon

   contains the main function files
   
* demon.pbs

   PBS file to hand in jobs
   
* hostfile
 
   hostfile for MPI
   
* myheader
 
   contains pipeline, mypdgemm, mapping functions
   
* sample_output.dat
 
   a output file of eepdgemm


#Paper:#
[Energy Efficient Parallel Matrix-Matrix Multiplication for DVFS-Enabled Clusters](http://ieeexplore.ieee.org/xpl/login.jsp?tp=&arnumber=6337486&url=http%3A%2F%2Fieeexplore.ieee.org%2Fxpls%2Fabs_all.jsp%3Farnumber%3D6337486)
